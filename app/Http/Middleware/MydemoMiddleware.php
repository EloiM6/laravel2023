<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class MydemoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    // Atenció !!! Recordar de posar al kernel el pseudònim:
    // 'mydemorol' => MydemoMiddleware::class,
    public function handle(Request $request, Closure $next): Response
    {
        $user = Auth::user();
        $otrouser = User::find($user->id);
        if($otrouser->idRolsEthan !=1){
            return redirect('SensePermisos');
        }

        return $next($request);
    }
}
