<?php

namespace App\Http\Controllers;

use App\Models\Figurants;
use App\Models\Judicis;
use App\Models\Rols;
use App\Models\FigurantsJudicis;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class MyDemoController extends Controller
{
    //Totes les funcions passar primer per saber si l'usuari està
    // autenticat
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function test1()
    {
       return Inertia::render('Demo2',['var1'=>'Variable1', 'var2'=>'Variable2']);
    }

    public function getAllRols()
    {
        $tots = Rols::all();
        return $tots->toJson();
    }
    public function getAllRols2()
    {
        $tots = Rols::all();
        return Inertia::render('LListarRols',['rols'=>$tots]);
    }

    public function addRols(Request $request)
    {
        $rol = new Rols();
        $rol->nom = $request->nom;
        $rol->save();
        return redirect('demo4');
    }
    public function editRols(Request $request)
    {
        $rol = Rols::find($request->selectedOption);
        $rol->nom = $request->nom;
        $rol->save();
        return redirect('demo4');
    }
    public function removeRols(Request $request)
    {
        Rols::destroy($request->selectedOption);
        return redirect('demo4');
    }

    public function getAllRols3()
    {
        $tots = Rols::all();
        return Inertia::render('ActualitzarRols',['rols'=>$tots]);
    }

    public function filterRols(Request $request){
        // https://laravel.com/docs/10.x/queries
        //$rols = Rols::where('nom', $request->nom)->get();
        //$rols = Rols::where('nom','>', $request->nom)->get();
        $rols = Rols::where('nom', 'LIKE', '%' . $request->nom . '%')->get();

        return Inertia::render('LListarRols',['rols'=>$rols]);
    }

    public function dadesUsuari()
    {
        //Check -> Verifica si s'ha fet login
        $bool = Auth::check();
        // Obtenim el ID de l'usuari
        $userId = Auth::id();
        // Obtenim totes les dades d'usuari
        $user = Auth::user();
        //return $bool;
        //return $userId;

        return $user->toJson();
    }

    public function getAllUsers(Request $request){
        $rols=Rols::all();
        $users=User::all();
        return Inertia::render('GetUsersRols', ['users'=>$users,'rols'=>$rols]);
    }

    public function getAllUsersByRol(Request $Request)
    {
        $rol = $Request->selectedOption;
        $rols=Rols::all();
        $users=User::where('idRolsEthan',$rol)->get();

        //return $rol;
        return Inertia::render('GetUsersRols', ['users'=>$users,'rols'=>$rols]);

    }

    public function getAllUsersByRol2()
    {
        $users = User::addSelect(['rol'=> Rols::select('nom')
            ->whereColumn('idRols','users.idRolsEthan')])->get();

        return Inertia::render('LlistatUsuaris',['users'=>$users]);
    }

    public function getAllUsersByRol3(Request $Request)
    {
        $rols = Rols::all();
        $user = User::find($Request->id);

        return Inertia::render('ActualizarUsuariRol',['user'=>$user,'rols' => $rols]);
    }

    public function SetUserRol(Request $Request) {
        $user = User::find($Request->id);
        $user->idRolsEthan = $Request->selectedOption;
        $user->Save();
        return redirect('demo10');
    }
    public function joinJudiciFigurant()
    {
        $figurant=Figurants::find(1);
        $judici=Judicis::find(1);
        $a=$judici->FigurantsJudicis()->attach($figurant,['veredicte'=>'No culpable']);
        return $a;
    }

    public function SaveFigurant(Request $Request)
    {
        //validacion, si no la cumple peta
        $figurant=Figurants::find($Request->id);
        if($Request->cv !=null){
            $Request->validate([
                //el campo file es obligatorio, debe ser extension pdf xlx o csv, tamaño maximo 2MB
                'cv' => 'required|mimes:pdf,txt,doc,docx|max:1024',
            ]);
            $fileName2 = time().'_cv.'.$Request->cv->extension();
            $Request->cv->move(public_path('uploads'), $fileName2);
            $figurant->cv = $fileName2;
        }

        if($Request->foto !=null) {

            $fileName = time() . '_foto.' . $Request->foto->extension();
            $Request->foto->move(public_path('uploads'), $fileName);
            $figurant->foto = $fileName;
        }

        $figurant->Save();
        //$figurant=Figurants::find($Request->id);
        $figurants = Figurants::all();
        return Inertia::render('LlistatFigurants',['figurants'=> $figurants]);
    }

    public function getAllFigurants()
    {
        $figurants = Figurants::all();
        return Inertia::render('LlistatFigurants',['figurants'=> $figurants]);
    }

    public function editFigurantFiles(Request $Request){
        $figurant = Figurants::find($Request->id);
        return Inertia::render('EditFigurantFiles',['figurant'=> $figurant]);
    }

}
