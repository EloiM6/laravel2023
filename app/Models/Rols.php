<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rols extends Model
{
    use HasFactory;

    protected $table="rols";
    protected $primaryKey="idRols";
    protected $fillable=["idRols","nom"];
    //OneToMany
    public function users()
    {
        return $this->hasMany(User::class);
    }
}

