<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Judicis extends Model
{
    use HasFactory;
    protected $table = "judicis";
    protected $primaryKey = "idJudicis";
    protected $fillable = ["idFigurants","nom","descripcio"];
    public function FigurantsJudicis()
    {
        return $this->belongsToMany(Figurants::class, 'figurants_judicis', 'idJudicis','idFigurants')->using(FigurantsJudicis::class)->withTimestamps();
    }
}
