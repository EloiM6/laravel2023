<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Figurants extends Model
{
    use HasFactory;
    protected $table = "figurants";
    protected $primaryKey = "idFigurants";
    protected $fillable = ["idFigurants","nom","cognoms","foto","cv"];

    public function FigurantsJudicis(){
        return $this->belongsToMany(Judicis::class, 'figurants_judicis', 'idFigurants','idJudicis')->using(FigurantsJudicis::class)->withTimestamps();
    }

}
