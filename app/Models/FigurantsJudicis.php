<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class FigurantsJudicis extends Pivot
{
    use HasFactory;
    protected $table = "figurants_judicis";
    protected $primaryKey = "idFigjud";
    protected $fillable = ["idFigjud","idFigurants","idJudicis","culpable","veredicte"];
}
