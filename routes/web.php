<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});
Route::get('/SensePermisos', function (){
   return Inertia::render('SensePermisos');
})->middleware('auth')->name('SensePermisos');

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/demo', function (){
    return Inertia::render('prueba1');
} );
Route::get('/demo2',[\App\Http\Controllers\MyDemoController::class, 'test1']);
Route::get('/demo3',[\App\Http\Controllers\MyDemoController::class, 'getAllRols']);
// Afegim ->middleware('auth') perquè no es pugui entrar si no ha fet login
Route::get('/demo4',[\App\Http\Controllers\MyDemoController::class, 'getAllRols2'])->middleware('auth')->name('prova4');
Route::get('/demo5', function (){
    return Inertia::render('AfegirRols');
});


Route::get('/demo6',[\App\Http\Controllers\MyDemoController::class, 'getAllRols3']);

Route::get('/demo7', function (){
    return Inertia::render('FilterRols');
});

Route::post('/AddRols',[\App\Http\Controllers\MyDemoController::class, 'addRols'])->name('AddRols');
Route::post('/EditRols',[\App\Http\Controllers\MyDemoController::class, 'editRols'])->name('EditRols');
Route::post('/RemoveRols',[\App\Http\Controllers\MyDemoController::class, 'removeRols'])->name('RemoveRols');
Route::get('/demo7', function (){
    return Inertia::render('FilterRols');
})->middleware('mydemorol');
Route::post('/FilterRols',[\App\Http\Controllers\MyDemoController::class, 'filterRols'])->name('Filter');
Route::get('/demo8',[\App\Http\Controllers\MyDemoController::class,'dadesUsuari']);
Route::get('/demo9',[\App\Http\Controllers\MyDemoController::class, 'getAllUsers']);
Route::post('/GetAllUsersByRol', [\App\Http\Controllers\MyDemoController::class,'getAllUsersByRol'])->name('GetAllUsersByRol');

Route::get('/demo10', [\App\Http\Controllers\MyDemoController::class,'getAllUsersByRol2'])->middleware('mydemorol');
Route::post('/demo11', [\App\Http\Controllers\MyDemoController::class,'getAllUsersByRol3'])->name('EditUserRol');
Route::post('/SetUserRol', [\App\Http\Controllers\MyDemoController::class,'SetUserRol']) ->name('SetUserRol');
Route::get('/prova12',[\App\Http\Controllers\MyDemoController::class,'joinJudiciFigurant']);

Route::get('/prova13', [\App\Http\Controllers\MyDemoController::class,'getAllFigurants'])->name('LlistarFigurants');
Route::post('/prova13', [\App\Http\Controllers\MyDemoController::class,'editFigurantFiles'])->name('EditFigurantFiles');

Route::post("/prova14",[\App\Http\Controllers\MyDemoController::class,'SaveFigurant'])->name('SaveFigurant');
require __DIR__.'/auth.php';
