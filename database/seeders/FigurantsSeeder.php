<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FigurantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('figurants')->insert([
            'nom' => 'Lucas 1',
            'cognoms' => 'Lopez',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('figurants')->insert([
            'nom' => 'Lucas 2',
            'cognoms' => 'Lopez',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('figurants')->insert([
            'nom' => 'Lucas 3',
            'cognoms' => 'Lopez',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
