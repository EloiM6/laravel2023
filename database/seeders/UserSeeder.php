<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'eloi',
            'password' => Hash::make('super3'),
            'idRolsEthan' => 1,
            'email' => 'eloi@correu.cat',
            'email_verified_at' => now(),
            'created_at' => now(),
        ]);
        DB::table('users')->insert([
            'name' => 'demo',
            'password' =>Hash::make('super3'),
            'idRolsEthan' => 1,
            'email' => 'demo@correu.cat',
            'email_verified_at' => now(),
            'created_at' => now(),
        ]);
    }
}
