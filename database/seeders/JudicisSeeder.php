<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JudicisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('judicis')->insert([
            'nom' => 'Robatori de carregador',
            'descripcio' => 'Algú es deixa el carregador a classe',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('judicis')->insert([
            'nom' => 'Robatori de ratolí',
            'descripcio' => 'Algú es deixa el ratolí a classe',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('judicis')->insert([
            'nom' => 'Robatori Identitat',
            'descripcio' => "Algú entra al correu d'un altre company de classe i li diu que és el jo del futur.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
