<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('figurants', function (Blueprint $table) {
            $table->bigIncrements("idFigurants");
            $table->string("nom",30);
            $table->string("cognoms",30);
            $table->text('foto')->nullable(true);
            $table->text('cv')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('figurants');
    }
};
