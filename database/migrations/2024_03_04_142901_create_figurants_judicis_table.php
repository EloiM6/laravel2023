<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('figurants_judicis', function (Blueprint $table) {

            $table->bigIncrements("idFigjud");
            $table->foreignId('idFigurants')->nullable()->constrained('figurants')->references('idFigurants');
            $table->foreignId('idJudicis')->nullable()->constrained('judicis')->references('idJudicis');
            $table->boolean('culpable')->default(false);
            $table->text('veredicte');
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('figurants_judicis', function (Blueprint $table) {
            $table->dropForeign(['figurants_judicis_idFigurant_foreign']);
            $table->dropColumn('idFigurant');
            $table->dropForeign(['figurants_judicis_idJudici_foreign']);
            $table->dropColumn('idJudici');
        });
        Schema::dropIfExists('figurants_judicis');
    }
};
